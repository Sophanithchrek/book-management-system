package com.example.demo.Repository.Provider;

import com.example.demo.Repository.Model.User;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {
    public String insertUser(@Param("user") User user){
        return new SQL(){{
            INSERT_INTO("users");
            VALUES("username,email,pwd,status","#{user.username},#{user.email},#{user.pwd},#{user.status}");
        }}.toString();

    }
}
