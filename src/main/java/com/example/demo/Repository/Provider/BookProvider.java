package com.example.demo.Repository.Provider;

import com.example.demo.Repository.Model.BookFilter;
import com.example.demo.Utilities.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String findAllWithFilter(@Param("filter")BookFilter bookFilter, @Param("paging") Paging paging){
        return new SQL(){{
            SELECT("b.*,c.title");
            FROM("tb_book b");
            INNER_JOIN("tb_category c on b.category_id=c.id");

            if(bookFilter.getName()!=null)
                WHERE("b.name ILIKE '%'||#{filter.name}||'%'");

            if(bookFilter.getCategoryId()!=null)
                WHERE("b.category_id = #{filter.categoryId}");

            ORDER_BY("b.id ASC limit #{paging.limit} offset #{paging.offset}");
        }}.toString();
    }

    public String countAllBook(BookFilter filter){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_book");
            if(filter.getName()!=null)
                WHERE("name ILIKE '%'||#{filter.name}||'%'");

            if(filter.getCategoryId()!=null)
                WHERE("category_id = #{filter.categoryId}");

        }}.toString();
    }
}
