package com.example.demo.Repository.Provider;

import com.example.demo.Utilities.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String findAll(@Param("paging")Paging paging){
        return new SQL(){{
            SELECT("*");
            FROM("tb_category");
            ORDER_BY("id ASC limit #{paging.limit} offset #{paging.offset}");
        }}.toString();
    }
    public String countAll(){
        return new SQL(){{
            SELECT("COUNT(id)");
            FROM("tb_category");
        }}.toString();
    }
}
