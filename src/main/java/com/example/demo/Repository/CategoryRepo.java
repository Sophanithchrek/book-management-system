package com.example.demo.Repository;

import com.example.demo.Repository.Model.Category;
import com.example.demo.Repository.Provider.CategoryProvider;
import com.example.demo.Utilities.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface CategoryRepo {
    @Insert("insert into tb_category (title) values(#{category.name})")
    Boolean insert(@Param("category") Category category);
    @Delete("delete from tb_category where tb_category.id=#{id}")
    Boolean delete(int id);
    @Update(("update tb_category set title=#{category.name} where id=#{category.id}"))
    Boolean update(@Param("category") Category category);
    @SelectProvider(type = CategoryProvider.class,method = "findAll")
    @Results({
            @Result(property = "name",column = "title")
    })
    List<Category> findAll(@Param("paging")Paging paging);
    @Select("select * from tb_category where id=#{id}")
    @Results({
            @Result(property = "name",column = "title")
    })
    Category findOne(int id);
    @SelectProvider(type = CategoryProvider.class,method = "countAll")
    Integer countAll();
}
