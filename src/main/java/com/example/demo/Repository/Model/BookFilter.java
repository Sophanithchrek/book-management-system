package com.example.demo.Repository.Model;

public class BookFilter {
    String name;
    Integer categoryId;

    public BookFilter() {
    }

    public BookFilter(String name, Integer categoryId) {
        this.name = name;
        this.categoryId = categoryId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "BookFilter{" +
                "name='" + name + '\'' +
                ", categoryId=" + categoryId +
                '}';
    }
}
