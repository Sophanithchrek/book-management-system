package com.example.demo.Repository;

import com.example.demo.Repository.Model.Book;
import com.example.demo.Repository.Model.BookFilter;
import com.example.demo.Repository.Provider.BookProvider;
import com.example.demo.Utilities.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface BookRepo {
    @Insert("insert into tb_book (name,author,description,thumbnail,category_id) values (#{name},#{author},#{description},#{thumbnail},#{category.id})")
    Boolean insert(Book book);

    @Delete("delete from tb_book where tb_book.id=#{id}")
    Boolean delete(int id);
    @Update("update tb_book set name=#book.name},author=#{book.author},category_id=#{book.category.id},thumbnail=#{book.thumbnail},description=#{book.description} where id=#{id}")
    Boolean update(Book book);


    @Select( "select b.*,c.title  from tb_book b inner join tb_category c on b.category_id=c.id order by b.id asc")
    @Results({

            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "title")
    })
    List<Book> findAll();

    @Select("select b.*,c.title  from tb_book b inner join tb_category c on b.category_id=c.id where b.id=#{id}")
    @Results({

            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "title")
    })
    Book findOne(int id);
    @SelectProvider(type = BookProvider.class,method = "countAllBook")
    Integer countAll(@Param("filter") BookFilter filter);
    @SelectProvider(type = BookProvider.class,method = "findAllWithFilter")
    @Results({

            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.name",column = "title")
    })
    List<Book> findAllWithFilter(@Param("filter")BookFilter filter, @Param("paging")Paging paging);
}
