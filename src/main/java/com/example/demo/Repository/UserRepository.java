package com.example.demo.Repository;

import com.example.demo.Repository.Model.Role;
import com.example.demo.Repository.Model.User;
import com.example.demo.Repository.Provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository {
    @Select("select * from users u where u.email  = #{email}")
    @Results({
            @Result(property = "id",column = "id"),
            @Result(property = "roles",column = "id",many = @Many(select = "findRolesByUserId"))
    })
    User findByEmail(String email);
    @Select("SELECT r.id, r.name FROM tb_roles r INNER JOIN tb_users_roles ur ON r.id= ur.role_id WHERE ur.user_id =#{id}")
    List<Role> findRolesByUserId(int id);



@InsertProvider(type = UserProvider.class,method = "insertUser")
    boolean insertUser(@Param("user") User user);
}
