package com.example.demo.RestController;

import com.example.demo.Configuration.JwtConfiguration.JwtTokenUtil;
import com.example.demo.Repository.Model.JwtResponse;
import com.example.demo.Repository.Model.User;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class JwtRestController {
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    UserService userService;
    @Autowired
    JwtTokenUtil jwtTokenUtil;
    @Autowired
    BCryptPasswordEncoder encoder;



    @PostMapping("authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody User user)throws Exception {
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(user.getEmail(),user.getPwd()));
        }
        catch (BadCredentialsException e){
            throw new Exception("Incorrect email or password",e);

        }
        final UserDetails userDetails = userService.loadUserByUsername(user.getEmail());
        final String token = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new JwtResponse(token));
    }









    //    @GetMapping()
//    public String user(){
//        return userService.findByEmail("admin@gmail.com").toString();
//    }
//
//    @GetMapping("users")
//    public void insert() {
//        System.out.println(encoder.encode("123"));
//    }


//@GetMapping("hello")
//    public String hello(){
//        return "Hello Muah World";
//    }

}
