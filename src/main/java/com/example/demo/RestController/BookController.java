package com.example.demo.RestController;

import com.example.demo.Repository.Model.Book;
import com.example.demo.Repository.Model.BookFilter;
import com.example.demo.Repository.BookRepo;
import com.example.demo.Service.BookService;
import com.example.demo.Utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/books")
public class BookController {
    private final BookRepo bookRepo;

    public BookController(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }
    private BookService bookService;
@Autowired
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @PostMapping()
    public ResponseEntity<Map<String,Object>> insert(@RequestBody Book book){
        Map<String, Object>res = new HashMap<>();
        if(bookService.insert(book)){
            res.put("status",true);
            res.put("message","Success");
            return  new ResponseEntity<>(res,HttpStatus.CREATED);
        }else{
            res.put("status",false);
            res.put("message","No Content");
            return new ResponseEntity<>(res,HttpStatus.NO_CONTENT);
        }


    }
    @GetMapping()
    public ResponseEntity<Map<String,Object>> findAll(BookFilter filter, Paging paging) {
   Map<String, Object> response = new HashMap<>();
   List<Book> bookList = bookService.findAllWithFilter(filter,paging);
        System.out.println(bookList.toString());
       if(bookList.isEmpty()){
           response.put("message","No Data");
       }else
       {
           response.put("data",bookList);
           response.put("message",HttpStatus.OK);
           response.put("pagination",paging);
       }
       return ResponseEntity.ok(response);


    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Map<String, Object>> deleteBook(@PathVariable int id) {
    Map<String, Object> response = new HashMap<>();

        if(bookService.delete(id))
            response.put("message",HttpStatus.OK);
        else response.put("message",HttpStatus.NO_CONTENT);
        return  ResponseEntity.ok(response);

    }
    @GetMapping("/{id}")
    public ResponseEntity<Map<String,Object>> findOne(@PathVariable int id){
    Map<String, Object>res = new HashMap<>();
    Book b = bookService.findOne(id);
    if(b==null){
        res.put("message","No book is found");
        return ResponseEntity.notFound().build();
    }else{
        res.put("status",true);
        res.put("message","success");
        res.put("data",b);
        return ResponseEntity.status(HttpStatus.OK).body(res);
    }
    }


    @PutMapping("/{id}")
    public ResponseEntity<Map<String,Object>> update(@RequestBody Book book){
        Map<String, Object>res = new HashMap<>();
        if(bookService.update(book)){
            res.put("status",true);
            res.put("message","Success");
            return  new ResponseEntity<>(res,HttpStatus.OK);
        }else{
            res.put("status",false);
            res.put("message","No Content");
            return new ResponseEntity<>(res,HttpStatus.NO_CONTENT);
        }


    }
}
