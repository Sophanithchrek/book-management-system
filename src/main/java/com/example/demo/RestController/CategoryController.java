package com.example.demo.RestController;

import com.example.demo.Repository.Model.Category;
import com.example.demo.Service.CategoryService;
import com.example.demo.Utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("categories")
public class CategoryController {
    private CategoryService categoryService;

    @Autowired
    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping()
    public ResponseEntity<Map<String, Object>> findAll(Paging paging) {
        Map<String, Object> res = new HashMap<>();
        List<Category> categories = categoryService.findAll(paging);
        if ( categories.isEmpty() ) {
            res.put("status", false);
            res.put("message", "NO data");
            return new ResponseEntity<>(res, HttpStatus.NOT_FOUND);
        } else {
            res.put("status", true);
            res.put("message", "Success");
            res.put("data", categories);
            res.put("pagination", paging);
            return ResponseEntity.ok(res);
        }

    }

    @DeleteMapping("{id}")
    public ResponseEntity<Map<String, Object>> delete(@PathVariable("id") int id) {
        Map<String, Object> res = new HashMap<>();
        if ( categoryService.delete(id) ) {
            res.put("message", "success");
            return ResponseEntity.ok(res);
        } else {
            res.put("message", "sorry");
            return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
    @PostMapping()
    public ResponseEntity<Map<String,Object>> insert(@RequestBody Category category){
        Map<String, Object>res = new HashMap<>();
        if(categoryService.insert(category)){
            res.put("status",true);
            res.put("message","Success");
            return  new ResponseEntity<>(res,HttpStatus.CREATED);
        }else{
            res.put("status",false);
            res.put("message","No Content");
            return new ResponseEntity<>(res,HttpStatus.NO_CONTENT);
        }


    }
    @GetMapping("/{id}")

    @PutMapping("/{id}")
    public ResponseEntity<Map<String,Object>> update(@RequestBody Category category){
        Map<String, Object>res = new HashMap<>();
        if(categoryService.update(category)){
            res.put("status",true);
            res.put("message","Success");
            return  new ResponseEntity<>(res,HttpStatus.OK);
        }else{
            res.put("status",false);
            res.put("message","No Content");
            return new ResponseEntity<>(res,HttpStatus.NO_CONTENT);
        }


    }

}
