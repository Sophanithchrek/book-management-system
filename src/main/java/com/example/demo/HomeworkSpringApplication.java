package com.example.demo;

import com.example.demo.Service.upload.FilesStorageServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication

public class HomeworkSpringApplication implements CommandLineRunner {
    @Autowired
FilesStorageServiceImpl filesStorageService;
    public static void main(String[] args) {
        SpringApplication.run(HomeworkSpringApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        //filesStorageService.deleteAll();
//filesStorageService.init();

    }

    @Bean
    public BCryptPasswordEncoder bCryptPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }

}
