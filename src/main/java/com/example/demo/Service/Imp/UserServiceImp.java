package com.example.demo.Service.Imp;

import com.example.demo.Repository.Model.Role;
import com.example.demo.Repository.Model.User;
import com.example.demo.Repository.UserRepository;
import com.example.demo.Service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserServiceImp  implements UserService {
    private UserRepository userRepository;
@Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    @Override
    public List<Role> findRolesByUserId(int id) {
        return null;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

    User user = this.userRepository.findByEmail(email);

//        Set<GrantedAuthority> grantedAuthorities = new HashSet< >();
//        grantedAuthorities.add(new SimpleGrantedAuthority("USER"));
//        grantedAuthorities.add(new SimpleGrantedAuthority("ADMIN"));

//        System.out.println(user.toString());
//        System.out.println(Arrays.toString(user.getRoles().toArray()));
//        System.out.println(user.getPwd());
        return user;
    }

    @Override
    public boolean insert(User user) {
        return this.userRepository.insertUser(user);
    }
}
