package com.example.demo.Service.Imp;

import com.example.demo.Repository.Model.Category;
import com.example.demo.Repository.CategoryRepo;
import com.example.demo.Service.CategoryService;
import com.example.demo.Utilities.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {
    private CategoryRepo categoryRepo;
@Autowired
    public CategoryServiceImp(CategoryRepo categoryRepo) {
        this.categoryRepo = categoryRepo;
    }

    @Override
    public Boolean insert(Category category) {
        return categoryRepo.insert(category);
    }

    @Override
    public Boolean delete(int id) {
        return categoryRepo.delete(id);
    }

    @Override
    public Boolean update(Category category) {
        return categoryRepo.update(category);
    }

    @Override
    public List<Category> findAll(Paging paging) {
        paging.setTotalCount(categoryRepo.countAll());
        return categoryRepo.findAll(paging);
    }

    @Override
    public Category findOne(int id) {
        return categoryRepo.findOne(id);
    }
}
