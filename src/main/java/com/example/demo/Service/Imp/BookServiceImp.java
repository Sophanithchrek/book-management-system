package com.example.demo.Service.Imp;

import com.example.demo.Repository.Model.Book;
import com.example.demo.Repository.Model.BookFilter;
import com.example.demo.Repository.BookRepo;
import com.example.demo.Service.BookService;
import com.example.demo.Utilities.Paging;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImp implements BookService {
    private BookRepo bookRepo;

    public BookServiceImp(BookRepo bookRepo) {
        this.bookRepo = bookRepo;
    }

    @Override
    public Boolean insert(Book book) {
        return bookRepo.insert(book);
    }

    @Override
    public Boolean delete(int id) {
        return bookRepo.delete(id);
    }

    @Override
    public Boolean update(Book book) {
        return bookRepo.update(book);
    }

    @Override
    public List<Book> findAll() {
        return bookRepo.findAll();
    }

    @Override
    public Book findOne(int id) {
        return bookRepo.findOne(id);
    }

    @Override
    public Integer countAll(BookFilter filter) {
        return bookRepo.countAll(filter);
    }

    @Override
    public List<Book> findAllWithFilter(BookFilter filter, Paging paging) {
        paging.setTotalCount(bookRepo.countAll(filter));
        return bookRepo.findAllWithFilter(filter, paging);
    }
}
