package com.example.demo.Service;

import com.example.demo.Repository.Model.Role;
import com.example.demo.Repository.Model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {
    User findByEmail(String email);
    boolean insert(User user);
    List<Role> findRolesByUserId(int id);

}
