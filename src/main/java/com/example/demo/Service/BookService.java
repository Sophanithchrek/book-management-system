package com.example.demo.Service;

import com.example.demo.Repository.Model.Book;
import com.example.demo.Repository.Model.BookFilter;
import com.example.demo.Utilities.Paging;

import java.util.List;

public interface BookService {

    Boolean insert(Book book);
    Boolean delete(int id);
    Boolean update(Book book);
    List<Book> findAll();

    Book findOne(int id);

    Integer countAll(BookFilter filter);

    List<Book> findAllWithFilter(BookFilter filter, Paging paging);
}
