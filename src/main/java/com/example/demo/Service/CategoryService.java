package com.example.demo.Service;

import com.example.demo.Repository.Model.Category;
import com.example.demo.Utilities.Paging;

import java.util.List;

public interface CategoryService {

    Boolean insert(Category category);

    Boolean delete(int id);

    Boolean update(Category category);

    List<Category> findAll(Paging paging);

    Category findOne(int id);
}
