create table if not exists public.tb_category
(
	id serial not null
		constraint tb_category_pkey
			primary key,
	title varchar(100) not null
);

alter table public.tb_category owner to postgres;

create table if not exists public.users
(
	id serial not null
		constraint tb_user_pk
			primary key,
	username varchar(100) not null,
	email varchar(70),
	pwd varchar(100) not null,
	status boolean
);

alter table public.users owner to postgres;

create unique index if not exists tb_user_username_uindex
	on public.users (username);

create unique index if not exists tb_user_email_uindex
	on public.users (email);

create table if not exists public.tb_roles
(
	id smallserial not null
		constraint table_roles_pk
			primary key,
	name varchar(50) not null
);

alter table public.tb_roles owner to postgres;

create unique index if not exists table_roles_name_uindex
	on public.tb_roles (name);

create table if not exists public.tb_users_roles
(
	id bigserial not null
		constraint table_users_roles_pk
			primary key,
	user_id integer
		constraint table_users_roles_tb_user_id_fk
			references public.users,
	role_id smallint
		constraint table_users_roles_table_roles_id_fk
			references public.tb_roles
);

alter table public.tb_users_roles owner to postgres;

create table if not exists public.tb_book
(
	id serial not null
		constraint tb_book_pkey
			primary key,
	name varchar(50) not null,
	author varchar(50) not null,
	description text not null,
	thumbnail varchar not null,
	category_id integer not null
		constraint tb_book_category_id_fkey
			references public.tb_category
				on update cascade on delete cascade
);

alter table public.tb_book owner to postgres;

create table if not exists public.articles
(
	id serial not null
		constraint articles_pk
			primary key,
	"articleId" varchar(100) not null,
	title varchar(50) not null,
	description text
);

alter table public.articles owner to postgres;

create unique index if not exists articles_article_id_uindex
	on public.articles ("articleId");
